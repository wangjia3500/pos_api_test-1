import unittest
import os

# case path
case_path = os.path.join(os.getcwd(), "test_cases")
def all_Cases():
    discover = unittest.defaultTestLoader.discover(case_path, pattern="*test.py",
                                                                  top_level_dir=None)
    print(discover)
    return discover

if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(all_Cases())