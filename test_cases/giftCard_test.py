#coding=utf-8
import unittest
import pymysql
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client
import time
import datetime

class GiftCardTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    @parameterized.expand([
        param("999", "testcard1")
    ])
    def test_create_GiftCard(self, giftCardNo, giftCardName):
        """
        giftCardNo: 礼品卡ID
        giftCardName: 礼品卡名称
        创建礼品卡
        :return: None
        """
        self.logger.info("enter create")
        createTime = datetime.datetime.now()
        createuserApi = {"giftCard": {"number": giftCardNo, "name": giftCardName, "expireTime": "2099-12-31",
                                      "balance": "0",
                                      "value": "0", "enabled": "1", "createTime": createTime,
                                      "cardType": "PHYISCAL"},
                         "userAuth_gift": {"userId": "1", "sessionKey": self.sessionKey},}

        giftCard_temp = createuserApi["giftCard"]
        userAuth_gift_temp = createuserApi["userAuth_gift"]

        res_createGiftCard = self.client.service.SaveGiftCard(giftCard=giftCard_temp, userAuth=userAuth_gift_temp)
        ParsedResponseGiftCard = fastest_object_to_dict(res_createGiftCard)

        self.giftCardId = ParsedResponseGiftCard['Result']['id']
        self.assertEqual(True, ParsedResponseGiftCard['Result']['successful'])
        # self.logger.debug(self.giftCardId)

    def test_recharge_giftcard(self):
        # 查找礼品卡
        # 连接数据库,查询要重置礼品卡的id
        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from gift_card where number='were';")
        # 礼品卡对应数据库字段 ： id, name, number, merchant_id, value, balance, cellphone, expire_time, customer_id,
        # enabled,created_on, last_updated, created_by

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        for d in data:
          giftCardId = d[0]
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"type": "GIFT_CARD", "totalPrice": 60, "userPassword": "11",
                       "orderItems":{"saleItemId": "908", "quantity": "1", "price": 60,
                                     "giftCard":{"id": giftCardId, "number": "were"}}},}
        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        # 发送保存订单请求
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])
        targetId = ParsedResponse['Result']['id']
        # 锁定任务
        resAddLock = self.client.service.AddLock(type="ORDER", targetId=targetId, userAuth=userAuth_temp)
        ParsedResponseAddLock = fastest_object_to_dict(resAddLock)
        self.logger.info(ParsedResponseAddLock)
        self.assertEqual(True, ParsedResponseAddLock['Result']['successful'])
        # 现金支付
        respulCashDrawer = self.client.service.PullCashDrawer(userId=1, userAuth=userAuth_temp)
        ParsedResponsepulCashDrawer = fastest_object_to_dict(respulCashDrawer)
        self.logger.info(ParsedResponsepulCashDrawer)
        self.assertEqual(True, ParsedResponsepulCashDrawer['successful'])
        paymentRecord = {"userId":1, "orderId": targetId, "type": "CASH", "amount": 60,
                                          "paidAmount":60, "multiplePayments": "false"}

        resSavePaymentRecord = self.client.service.SavePaymentRecord(printPaymentReceipt=True, merchantCopyOnly= False,
                                                                     paymentRecord=paymentRecord,
                                                                     userAuth=userAuth_temp)

        ParsedResponseSavePaymentRecord = fastest_object_to_dict(resSavePaymentRecord)
        self.logger.info(ParsedResponseSavePaymentRecord)
        self.assertEqual(True, ParsedResponseSavePaymentRecord['Result']['successful'])
        # 解除锁定任务
        resRemoveLock = self.client.service.RemoveLock(type="ORDER", targetId=targetId, userAuth=userAuth_temp)
        ParsedResponseRemoveLock = fastest_object_to_dict(resRemoveLock)
        self.logger.info(ParsedResponseRemoveLock)
        self.assertEqual(True, ParsedResponseRemoveLock['Result']['successful'])

    def test_update_GiftCard(self):
        # 更新名称为11的礼品卡的余额
        self.logger.info("enter update")
        createuserApi = {"userAuth_gift": {"userId": "1", "sessionKey": self.sessionKey},
                         "giftCard":{"id": 7, "number": "were", "balance": 500}}
        userAuth_gift_temp = createuserApi["userAuth_gift"]
        gift_temp = createuserApi["giftCard"]
        res1 = self.client.service.SaveGiftCard(giftCard=gift_temp, userAuth=userAuth_gift_temp)
        ParsedResponse = fastest_object_to_dict(res1)
        self.logger.info(ParsedResponse)

    def test_delete_GiftCard(self):
        self.logger.info("enter delete")
        createuserApi = {"userAuth_gift": {"userId": "1", "sessionKey": self.sessionKey}, }
        userAuth_gift_temp = createuserApi["userAuth_gift"]

        # 连接数据库,查询到礼品卡在数据库存储的ID
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from gift_card where number='999';")
        # 礼品卡对应数据库字段 ： id, name, number, merchant_id, value, balance, cellphone, expire_time, customer_id,
        # enabled,created_on, last_updated, created_by

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        for d in data:
            giftCardId = d[0]
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

        # 删除礼品卡，ID属于必填字段
        res_deleteGiftCard = self.client.service.DeleteGiftCard(id=giftCardId, userAuth=userAuth_gift_temp)
        ParsedResponseGiftCard = fastest_object_to_dict(res_deleteGiftCard)
        # self.logger.info(ParsedResponseGiftCard)
        self.assertEqual(True, ParsedResponseGiftCard['successful'])
        time.sleep(10)

        # 连接数据库,查询到礼品卡在数据库存储的ID
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from gift_card where number='999';")
        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        print(data)
        for d in data:
            # 获取礼品卡的是否Enable状态的标志位
            giftCardFlag = d[9]

        self.assertEqual(0, giftCardFlag)

        cur.execute("delete from gift_card where number='999';")
        conn.commit()
        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()