#coding=utf-8
import unittest
import time
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class OrderTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    def test_saveOrder_for_TOGO(self):
        """
        创建To Go订单
        :return:
        """
        now = int((time.time()) * 1000)
        # 订单参数
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"createTime": now, "type": "TOGO", "status": "ORDERED",
                                 "currentUserId": "1", "userId": "1", "taxExempt": "false",
                                 "numOfGuests": "2", "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.80",
                                 "roundingAmount": "0", "printTicketWhenVoid": "true",
                                 "orderTax": {"taxId": "1", "taxAmount": "0.8"}, "discount": "0", "charge": "0",
                                 "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                                           "orderItems": {"saleItemId": "924",
                                                                                          "seatId": "1",
                                                                                          "quantity": "1",
                                                                                          "originalSalePrice": "7.95",
                                                                                          "price": "7.95",
                                                                                          "status": "ORDERED",
                                                                                          "discount": "0",
                                                                                          "charge": "0"}}},}


        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def test_orderHeader(self):
        """
        验证订单头部的功能
        :return:
        """
        # 创建To Go订单
        now = int((time.time()) * 1000)
        # 订单参数
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"createTime": now, "type": "TOGO", "status": "ORDERED",
                                 "currentUserId": "1", "userId": "1", "taxExempt": "false",
                                 "numOfGuests": "1", "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.8",
                                 "roundingAmount": "0", "printTicketWhenVoid": "true",
                                 "orderTax": {"taxId": "1", "taxAmount": "0.8"}, "discount": "0", "charge": "0",
                                 "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                                           "orderItems":
                                                                               {
                                                                                   "saleItemId": "924",
                                                                                   "seatId": "1",
                                                                                   "quantity": "1",
                                                                                   "originalSalePrice": "7.95",
                                                                                   "price": "7.95",
                                                                                   "status": "ORDERED",
                                                                                   "discount": "0",
                                                                                   "charge": "0"
                                                                               },
                                                                           }},}

        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        orderID = ParsedResponse['Result']['id']
        self.logger.info(ParsedResponse)
        orderItemId = ParsedResponse['order']['subOrders'][0]['orderItems'][0]['id']

        # 更新订单
        updateTime = int((time.time()) * 1000)
        updated_order_param=\
            {
                "order": {
                    "createTime": updateTime,
                    "callerId": "false",
                    "type": "DINE_IN",
                    "id": orderID,
                    "status": "ORDERED",
                    "currentUserId": "1",
                    "notes": "22",
                    "userId": "76",
                    "tableId": "28",
                    "taxExempt": "false",
                    "numOfGuests": "5",
                    "totalPrice": "7.95",
                    "totalTips": "0",
                    "totalTax": "0.80",
                    "roundingAmount": "0",
                    "printTicketWhenVoid": "true",
                    "orderTax": {
                        "taxId": "1",
                        "taxAmount": "0.8"
                    },
                    "discountID": "-1",
                    "chargeID": "-1",
                    "discount": "0",
                    "charge": "0",
                    "loyaltyDiscount": "false",
                    "subOrders": {
                        "seatNum": "1",
                        "orderItems": {
                            "saleItemId": "924",
                            "id": orderItemId,
                            "seatId": "5866",
                            "quantity": "1",
                            "originalSalePrice": "7.95",
                            "price": "7.95",
                            "status": "ORDERED",
                            "discount": "0",
                            "charge": "0"
                        }
                    },
                    "productLine": "POS"
                },
        }
        resUpdateOrder = self.client.service.SaveOrder(order=updated_order_param['order'], userAuth=userAuth_temp)
        ParsedResponseUpdateOrder = fastest_object_to_dict(resUpdateOrder)
        self.logger.info(ParsedResponseUpdateOrder)

        # 结果校验
        self.assertEqual(True, ParsedResponseUpdateOrder['Result']['successful'])
        self.assertEqual(orderID, ParsedResponseUpdateOrder['Result']['id'])
        self.assertEqual("DINE_IN", ParsedResponseUpdateOrder['order']['type'])
        self.assertEqual("22", ParsedResponseUpdateOrder['order']['notes'])
        self.assertEqual(5, ParsedResponseUpdateOrder['order']['numOfGuests'])
        self.assertEqual('Michelle', ParsedResponseUpdateOrder['order']['serverName'])

    def test_order_items(self):
        """
        验证订单的菜品相关的功能 菜 增删改 添加调味
        :return:
        """
        # 创建Delivery订单
        createTime = int((time.time()) * 1000)
        order_param = { "userAuth": {"userId": "1", "sessionKey": self.sessionKey},
            "order": {
              "createTime": createTime,
              "callerId": "false",
              "type": "DELIVERY",
              "status": "ORDERED",
              "currentUserId": "1",
              "userId": "1",
              "taxExempt": "false",
              "numOfGuests": "1",
              "totalPrice": "10.9",
              "totalTips": "0",
              "totalTax": "1.09",
              "roundingAmount": "0",
              "printTicketWhenVoid": "true",
              "orderTax": {
                "taxId": "1",
                "taxAmount": "1.09"
              },
              "discountID": "-1",
              "chargeID": "-1",
              "discount": "0",
              "charge": "0",
              "loyaltyDiscount": "false",
              "subOrders": {
                "seatNum": "1",
                "orderItems": [
                  {
                    "saleItemId": "924",
                    "seatId": "1",
                    "quantity": "1",
                    "originalSalePrice": "7.95",
                    "price": "7.95",
                    "status": "ORDERED",
                    "discount": "0",
                    "charge": "0"
                  },
                  {
                    "saleItemId": "925",
                    "seatId": "1",
                    "quantity": "1",
                    "originalSalePrice": "2.95",
                    "price": "2.95",
                    "status": "ORDERED",
                    "discount": "0",
                    "charge": "0"
                  }
                ]
              },
              "customer": {
                "id": "13",
                "phone": { "id": "13" },
                "address": { "id": "6" }
              }
            },
        }

        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        orderID = ParsedResponse['Result']['id']
        self.logger.info(ParsedResponse)
        orderItemIdList = list()
        for i in range(len(ParsedResponse['order']['subOrders'][0]['orderItems'])):
            orderItemIdList.append(ParsedResponse['order']['subOrders'][0]['orderItems'][i]['id'])
        self.logger.info(orderItemIdList)

        updateTime = int((time.time()) * 1000)
        update_order_item_param = {
            "order": {
                "createTime": updateTime,
                "callerId": "false",
                "type": "DELIVERY",
                "id": orderID,
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "17.9",
                "totalTips": "0",
                "totalTax": "1.79",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "1.79"
                },
                "discountID": "-1",
                "chargeID": "-1",
                "discount": "0",
                "charge": "0",
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": [
                        {
                            "saleItemId": "925",
                            "id": orderItemIdList[1],
                            "seatId": "5889",
                            "quantity": "2",
                            "originalSalePrice": "2.95",
                            "price": "2.95",
                            "status": "ORDERED",
                            "discount": "0",
                            "charge": "0"
                        },
                        {
                            "saleItemId": "942",
                            "seatId": "5889",
                            "quantity": "1",
                            "originalSalePrice": "10",
                            "price": "10",
                            "status": "ORDERED",
                            "discount": "0",
                            "charge": "0",
                            "options": [
                                {
                                    "optionId": "3652",
                                    "optionName": "Ginger",
                                    "quantity": "1",
                                    "optionType": "GLOBAL",
                                    "price": "2.00"
                                },
                                {
                                    "optionName": "22",
                                    "quantity": "1",
                                    "optionType": "NOTE",
                                    "price": "0"
                                }
                            ]
                        },
                        {
                            "saleItemId": "924",
                            "id": orderItemIdList[0],
                            "seatId": "5889",
                            "quantity": "0",
                            "originalSalePrice": "7.95",
                            "price": "7.95",
                            "status": "ORDERED",
                            "discount": "0",
                            "charge": "0"
                        }
                    ]
                },
                "productLine": "POS"
            },
        }

        resUpdateOrderItems = self.client.service.SaveOrder(order=update_order_item_param['order'], userAuth=userAuth_temp)
        ParsedResponseUpdateOrderItems = fastest_object_to_dict(resUpdateOrderItems)
        self.logger.info(ParsedResponseUpdateOrderItems)

        # 结果校验
        self.assertEqual(True, ParsedResponseUpdateOrderItems['Result']['successful'])

    def test_orderButtom(self):
        """
        验证订单底部的功能按钮，更新折扣 加收 小费等
        :return:
        """
        # 创建Delivery订单
        createTime = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {
                           "createTime": createTime,
                           "callerId": "false",
                           "type": "DELIVERY",
                           "status": "ORDERED",
                           "currentUserId": "1",
                           "userId": "1",
                           "taxExempt": "false",
                           "numOfGuests": "1",
                           "totalPrice": "10.9",
                           "totalTips": "0",
                           "totalTax": "1.09",
                           "roundingAmount": "0",
                           "printTicketWhenVoid": "true",
                           "orderTax": {
                               "taxId": "1",
                               "taxAmount": "1.09"
                           },
                           "discountID": "-1",
                           "chargeID": "-1",
                           "discount": "0",
                           "charge": "0",
                           "loyaltyDiscount": "false",
                           "subOrders": {
                               "seatNum": "1",
                               "orderItems": [
                                   {
                                       "saleItemId": "924",
                                       "seatId": "1",
                                       "quantity": "1",
                                       "originalSalePrice": "7.95",
                                       "price": "7.95",
                                       "status": "ORDERED",
                                       "discount": "0",
                                       "charge": "0"
                                   },
                                   {
                                       "saleItemId": "925",
                                       "seatId": "1",
                                       "quantity": "1",
                                       "originalSalePrice": "2.95",
                                       "price": "2.95",
                                       "status": "ORDERED",
                                       "discount": "0",
                                       "charge": "0"
                                   }
                               ]
                           },
                           "customer": {
                               "id": "13",
                               "phone": {"id": "13"},
                               "address": {"id": "6"}
                           }
                       },
                       }

        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        orderID = ParsedResponse['Result']['id']
        self.logger.info(ParsedResponse)

        orderItemIdList = list()
        for i in range(len(ParsedResponse['order']['subOrders'][0]['orderItems'])):
            orderItemIdList.append(ParsedResponse['order']['subOrders'][0]['orderItems'][i]['id'])
        self.logger.info(orderItemIdList)

        updateTime = int((time.time()) * 1000)
        update_order_buttom_param = {
            "order": {
                "createTime": updateTime,
                "callerId": "false",
                "type": "DELIVERY",
                "id": orderID,
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "10.9",
                "totalTips": "5",
                "totalTax": "0.65",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "0.65"
                },
                "discountName": "50% Off",
                "discountID": "7",
                "chargeName": "Add 10 %",
                "chargeID": "3",
                "discount": "5.45",
                "charge": "1.09",
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": [
                        {
                            "saleItemId": "924",
                            "id": orderItemIdList[0],
                            "seatId": "5915",
                            "quantity": "1",
                            "originalSalePrice": "7.95",
                            "price": "7.95",
                            "status": "ORDERED",
                            "discount": "3",
                            "discountName": "$3 Off",
                            "discountID": "2",
                            "charge": "0"
                        },
                        {
                            "saleItemId": "925",
                            "id": orderItemIdList[1],
                            "seatId": "5915",
                            "quantity": "1",
                            "originalSalePrice": "2.95",
                            "price": "2.95",
                            "status": "ORDERED",
                            "discount": "0",
                            "charge": "3",
                            "chargeName": "Add $3",
                            "chargeID": "4"
                        }
                    ]
                },
                "productLine": "POS"
            },
        }

        resUpdateOrderButtom = self.client.service.SaveOrder(order=update_order_buttom_param['order'],
                                                            userAuth=userAuth_temp)
        ParsedResponseUpdateOrderButtom = fastest_object_to_dict(resUpdateOrderButtom)
        self.logger.info(ParsedResponseUpdateOrderButtom)

        # 结果校验
        self.assertEqual(True, ParsedResponseUpdateOrderButtom['Result']['successful'])
        self.assertEqual(4.95,
                         ParsedResponseUpdateOrderButtom['order']['subOrders'][0]['orderItems'][0]['totalAmount'])
        self.assertEqual(5.95,
                         ParsedResponseUpdateOrderButtom['order']['subOrders'][0]['orderItems'][1]['totalAmount'])
        self.assertEqual(5.00,
                         ParsedResponseUpdateOrderButtom['order']['totalTips'])

    def test_delete_order(self):
        """
        删单
        :return:
        """
        # 创建Delivery订单
        createTime = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {
                           "createTime": createTime,
                           "callerId": "false",
                           "type": "DELIVERY",
                           "status": "ORDERED",
                           "currentUserId": "1",
                           "userId": "1",
                           "taxExempt": "false",
                           "numOfGuests": "1",
                           "totalPrice": "10.9",
                           "totalTips": "0",
                           "totalTax": "1.09",
                           "roundingAmount": "0",
                           "printTicketWhenVoid": "true",
                           "orderTax": {
                               "taxId": "1",
                               "taxAmount": "1.09"
                           },
                           "discountID": "-1",
                           "chargeID": "-1",
                           "discount": "0",
                           "charge": "0",
                           "loyaltyDiscount": "false",
                           "subOrders": {
                               "seatNum": "1",
                               "orderItems": [
                                   {
                                       "saleItemId": "924",
                                       "seatId": "1",
                                       "quantity": "1",
                                       "originalSalePrice": "7.95",
                                       "price": "7.95",
                                       "status": "ORDERED",
                                       "discount": "0",
                                       "charge": "0"
                                   },
                                   {
                                       "saleItemId": "925",
                                       "seatId": "1",
                                       "quantity": "1",
                                       "originalSalePrice": "2.95",
                                       "price": "2.95",
                                       "status": "ORDERED",
                                       "discount": "0",
                                       "charge": "0"
                                   }
                               ]
                           },
                           "customer": {
                               "id": "13",
                               "phone": {"id": "13"},
                               "address": {"id": "6"}
                           }
                       },
                       }

        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        orderID = ParsedResponse['Result']['id']
        self.logger.info(ParsedResponse)

        orderItemIdList = list()
        for i in range(len(ParsedResponse['order']['subOrders'][0]['orderItems'])):
            orderItemIdList.append(ParsedResponse['order']['subOrders'][0]['orderItems'][i]['id'])
        self.logger.info(orderItemIdList)

        deleteTime = int((time.time()) * 1000)
        delete_order_param = {
            "order": {
              "createTime": deleteTime,
              "callerId": "false",
              "type": "DELIVERY",
              "id": orderID,
              "status": "CANCELED",
              "currentUserId": "1",
              "userId": "1",
              "taxExempt": "false",
              "numOfGuests": "1",
              "totalPrice": "10.9",
              "totalTips": "0",
              "totalTax": "1.09",
              "roundingAmount": "0",
              "printTicketWhenVoid": "true",
              "orderTax": {
                "taxId": "1",
                "taxAmount": "1.09"
              },
              "discount": "0",
              "charge": "0",
              "voidReason": "22",
              "loyaltyDiscount": "false",
              "subOrders": {
                "seatNum": "1",
                "orderItems": [
                  {
                    "saleItemId": "924",
                    "id": orderItemIdList[0],
                    "seatId": "5927",
                    "quantity": "1",
                    "originalSalePrice": "7.95",
                    "price": "7.95",
                    "status": "ORDERED",
                    "discount": "0",
                    "charge": "0"
                  },
                  {
                    "saleItemId": "925",
                    "id": orderItemIdList[1],
                    "seatId": "5927",
                    "quantity": "1",
                    "originalSalePrice": "2.95",
                    "price": "2.95",
                    "status": "ORDERED",
                    "discount": "0",
                    "charge": "0"
                  }
                ]
              },
            },
        }

        resDeleteOrder = self.client.service.SaveOrder(order=delete_order_param['order'], userAuth=userAuth_temp)
        ParsedResponseDelOrder = fastest_object_to_dict(resDeleteOrder)
        self.logger.info(ParsedResponseDelOrder)
