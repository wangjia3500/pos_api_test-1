#coding=utf-8
import unittest
import pymysql
import time
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from common.common import connect_sql
from suds.client import Client

class MenuTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    @parameterized.expand([
        param({"groupName": "auto-test-Lunch", "groupChineseName": "自动化测试组-午餐", "Time": 2}),
        param({"groupName": "auto-test-AllDay", "groupChineseName": "自动化测试组-全天", "Time": 1}),
        param({"groupName": "auto-test-Dinner", "groupChineseName": "自动化测试组-晚餐", "Time": 4}),
    ])
    def test_1_create_group(self, groupParam):
        """
        :param groupParam: Name, ChineseName, Gruop Hours
        :return:
        """
        # POS-15599 POS-15597  POS-15596 POS-15601
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": groupParam['groupName'],
            "nameCh": groupParam['groupChineseName'],
            "restaurantHourIds": [groupParam['Time']],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual(groupParam['groupName'], res['group']['name'])

    @parameterized.expand([
        param("auto-test-Lunch"),
        param("auto-test-AllDay"),
        param("auto-test-Dinner")
    ])
    def test_2_delete_group(self, groupName):
        """
        param: 待删除的groupName
        :return:
        """
        # 根据传入的groupName在数据库中查找到对应的groupId
        sql = "select * from menu_group where name=%s"
        data = connect_sql(sql, groupName)
        for d in data:
          groupId = d[0]
        self.logger.info(groupId)
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup/batch/delete'
        payload = {
            "groupIds": [groupId],
        }

        res = self.req.visit('DELETE', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual('OK', res['result'])
        # 查询数据库中group的delete标志位的值
        sql = "select * from menu_group where name=%s"
        dataDelete = connect_sql(sql, groupName)
        self.logger.info(dataDelete)
        for d in dataDelete:
            deleteFlag = dataDelete[0][8]
        self.assertEqual(1, deleteFlag)

        # 在数据库中删除新增的group
        sql = "delete from menu_group where id=%s"
        connect_sql(sql, groupId)


    def test_3_creat_category_without_option(self):
        """
        :param categoryParam: groupName,name
        :return:
        """
        # POS - 15612
        # 创建 group
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": "pos-test-group",
            "restaurantHourIds": [1],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual('pos-test-group', res['group']['name'])
        groupId = res['group']['id']
        self.logger.info(groupId)
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "menuId": 1,
            "name": "pos-test-category-without-option",
            "nameCh": "不带调味的类",
            "groupId": groupId,
            "taxIds": [1]  # [1]为10%的税
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual("pos-test-category-without-option", res['menuCategory']['name'])

    @parameterized.expand([
        param({"groupName": "pos-test-group", "categoryName": "pos-test-category-without-option"})
    ])
    def test_4_update_category_name(self, categoryParam):
        """
        :param categoryParam: 待修改category name 及category属于的group name
        :return:
        """
        # POS-15739
        # 查询groupId
        sql = "select * from menu_group where name = %s;"
        dataGroup = connect_sql(sql, categoryParam['groupName'])
        for d in dataGroup:
            groupId = d[0]
        self.logger.info(groupId)
        # 查询categoryId
        sql = "select * from menu_category where name = %s;"
        dataCategory = connect_sql(sql, categoryParam['categoryName'])
        categoryId = 0
        for tmp in dataCategory:
            categoryId = tmp[0]
        self.logger.info(categoryId)
        url = 'http://localhost:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "menuId": 1,
            "groupId": groupId,
            "name": "pos-test-category-update",
            "id": categoryId
        }
        resUpdateCategory = self.req.visit('PUT', url, json=payload)
        self.logger.info(resUpdateCategory)
        # 结果校验
        self.assertEqual('pos-test-category-update', resUpdateCategory['menuCategory']['name'])

    @parameterized.expand([
        param({"groupName": "pos-test-group", "categoryName": "pos-test-category-update"}),
    ])
    def test_5_delete_category(self, categoryParam):
        # POS-17005
        # 查询categoryId
        sql = "select * from menu_category where name = %s;"
        data = connect_sql(sql, categoryParam['categoryName'])
        for d in data:
            categoryId = d[0]

        url = 'http://localhost:22080/kpos/webapp/menu/abstractCategory/batch/delete'
        payload = {
            "categoryIds": [categoryId],
        }
        resDeleteCategory = self.req.visit('DELETE', url, json=payload)
        self.logger.info(resDeleteCategory)
        # 结果校验
        self.assertEqual('OK',resDeleteCategory['result'])
        # 查询数据库中category的delete标志位的值
        # menu_category 表结构： id,name,pos_name,description,thumb_path,created_on,last_updated,created_by,
        # last_updated_by,version,group_id,display_priority,read_only,deleted,color,short_name,category_type...
        sql = "select * from menu_category where id=%s"
        dataDelete = connect_sql(sql, categoryId)
        self.logger.info(dataDelete)
        for d in dataDelete:
            deleteFlag = dataDelete[0][13]
        self.assertEqual(1, deleteFlag)
        # 数据库中删除新增的资源
        sql = "delete from menu_category where name=%s"
        connect_sql(sql, categoryParam['categoryName'])
        sql = "delete from menu_group where name=%s"
        connect_sql(sql, categoryParam['groupName'])

    def test_6_create_no_promotion_category(self):
        """
        创建折扣不生效的类
        param: None
        :return:
        """
        # POS - 15735
        # 创建 group
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": "pos-test-group",
            "restaurantHourIds": [1],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual('pos-test-group', res['group']['name'])
        groupId = res['group']['id']
        self.logger.info(groupId)
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "menuId": 1,
            "applicableToOrderDiscount": False,
            "applicableToTriggerPromotion": False,
            "discountAllowed": False,
            "name": "pos-test-category-no-promotion",
            "nameCh": "不打折的类",
            "groupId": groupId,
            "taxIds": [1]  # [1]为10%的税
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual("pos-test-category-no-promotion", res['menuCategory']['name'])
        categoryId = res['menuCategory']['id']
        self.logger.info(categoryId)
        # sql 查询折扣相关的标志位
        # menu_category 表结构： id,name,pos_name,description,thumb_path,created_on,last_updated,created_by,
        # last_updated_by,version,group_id,display_priority,read_only,deleted,color,short_name,category_type,
        # report_group_id,require_category,course_id,applicable_to_order_discount,discount_allow...
        sql = "select * from menu_category where id=%s"
        data = connect_sql(sql, categoryId)
        self.logger.info(data)
        for d in data:
            discountAllow = data[0][21]
        self.assertEqual(0, discountAllow)

        # # 删除新增的资源
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取数据库中对应时间段的预约记录
        cur = conn.cursor()
        # 执行需要的sql语句
        # cur.execute("delete from menu_category where name=pos-test-group;")
        cur.execute("SET FOREIGN_KEY_CHECKS = 0;")
        cur.execute("delete from menu_group where name='pos-test-category-no-promotion';")
        cur.execute("delete from menu_group where name='pos-test-group';")
        cur.execute("SET FOREIGN_KEY_CHECKS = 1;")
        conn.commit()
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

    def test_7_create_category_with_option(self):
        """
       创建带option的类
       param: None
       :return:
       """
        # POS - 15733
        # 创建 group
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": "pos-test-group",
            "restaurantHourIds": [1],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual('pos-test-group', res['group']['name'])
        groupId = res['group']['id']
        self.logger.info(groupId)
        login_url = 'http://localhost:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "applicableToOrderDiscount": True,
            "applicableToTriggerPromotion": True,
            "discountAllowed": True,
            "menuId": 1,
            "name": "pos-test-category-with-option",
            "nameCh": "带option的类",
            "groupId": groupId,
            "options": [{
                "name": "option-test",
                "price": 1,
                "suboptions": [{
                    "name": "suboption-test",
                    "price": "2"
                }]
            }],
            "taxIds": [1]  # [1]为10%的税
        }

        res = self.req.visit('post', login_url, json=payload)
        self.logger.debug(res)
        self.assertEqual("pos-test-category-with-option", res['menuCategory']['name'])

        # # 删除新增的资源
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取数据库中对应时间段的预约记录
        cur = conn.cursor()
        # 执行需要的sql语句
        cur.execute("SET FOREIGN_KEY_CHECKS = 0;")
        cur.execute("delete from menu_category where name='pos-test-category-with-option';")
        cur.execute("delete from menu_group where name='pos-test-group';")
        cur.execute("SET FOREIGN_KEY_CHECKS = 1;")
        conn.commit()
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

    def test_8_item_in_no_option_category(self):
        """
        增加菜，菜属于不带option的类
        :return:
        """
        # POS-15647
        # 创建 group
        group_url = 'http://localhost:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": "pos-test-group",
            "restaurantHourIds": [1],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = self.req.visit('post', group_url, json=payload)
        self.logger.debug(res)
        self.assertEqual('pos-test-group', res['group']['name'])
        groupId = res['group']['id']
        self.logger.info(groupId)
        category_url = 'http://localhost:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "menuId": 1,
            "name": "pos-test-category-without-option",
            "nameCh": "不带调味的类",
            "groupId": groupId,
            "taxIds": [1]  # [1]为10%的税
        }

        resCategory = self.req.visit('post', category_url, json=payload)
        self.logger.debug(resCategory)
        self.assertEqual("pos-test-category-without-option", resCategory['menuCategory']['name'])
        categoryId = resCategory['menuCategory']['id']

        # 新建菜
        item_url = 'http://localhost:22080/kpos/webapp/menu/menuSaleItem'
        payload = {
            'groupId': groupId,
            "categoryId": categoryId,
            "name": "pos-test-common-item",
            "price": 30,
        }

        resItem = self.req.visit('post', item_url, json=payload)
        self.logger.debug(resItem)
        self.assertEqual("pos-test-common-item", resItem['item']['name'])
        itemId = resItem['item']['id']

        # 点新增加的菜
        now = int((time.time()) * 1000)
        # 订单参数
        order_param = {
            "order": {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "30",
                "totalTips": "0",
                "totalTax": "3.00",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "3"
                },
                "discountID": "-1",
                "chargeID": "-1",
                "discount": "0",
                "charge": "0",
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": itemId,
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "30",
                        "price": "30",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0"
                    }
                }
            },
            "userAuth": {
                "userId": "1",
                "sessionKey": self.sessionKey
            }
        }

        resSaveOrder = self.client.service.SaveOrder(order=order_param["order"], userAuth=order_param["userAuth"])
        ParsedResponse = fastest_object_to_dict(resSaveOrder)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

        # 删除新增的资源
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        cur = conn.cursor()
        # 执行需要的sql语句
        cur.execute("SET FOREIGN_KEY_CHECKS = 0;")
        cur.execute("delete from menu_item where name='pos-test-common-item';")
        cur.execute("delete from menu_category where name='pos-test-category-without-option';")
        cur.execute("delete from menu_group where name='pos-test-group';")
        cur.execute("SET FOREIGN_KEY_CHECKS = 1;")
        conn.commit()
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
