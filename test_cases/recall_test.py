import unittest
import pymysql
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.para_format_converse import fastest_object_to_dict
from suds.client import Client
import time
from system import POSSystem
from datetime import datetime, date, timedelta


middle = time.strftime("%Y-%m-%d", time.localtime())
STARTtime = middle + ' 06:00:00'
tomorrow = (date.today() + timedelta(days = 1)).strftime("%Y-%m-%d")
ENDtime = tomorrow + ' 06:00:00'

class RecallTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://192.168.2.239:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "HH",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']


        #员工登录
        dest_url = 'http://192.168.2.239:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive",
                  "Content-type":"text/xml;charset=UTF-8"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
            # 关闭连接
            self.req.close_session()

    @parameterized.expand([
        param(STARTtime, ENDtime)
    ])
    def test_recall_by_date(self, startTime, endTime):
        '''recall页面按日期查询，举例为今天'''
        now = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"createTime": now, "callerId": "false", "type": "TOGO", "status": "ORDERED",
                                 "currentUserId": "1", "userId": "1", "taxExempt": "false", "numOfGuests": "1",
                                 "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.80", "roundingAmount": "0",
                                 "printTicketWhenVoid": "true", "orderTax": {"taxId": "1", "taxAmount": "0.8"},
                                 "discountID": "-1", "chargeID": "-1", "discount": "0", "charge": "0",
                                 "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                                           "orderItems": {"saleItemId": "924",
                                                                                          "seatId": "1",
                                                                                          "quantity": "1",
                                                                                          "originalSalePrice": "7.95",
                                                                                          "price": "7.95",
                                                                                          "status": "ORDERED",
                                                                                          "discount": "0",
                                                                                          "charge": "0"}}}}
        systemHandle = POSSystem()
        systemHandle.saveOrder(order_param)
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s;"
        cur.execute(sql, (STARTtime, ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql=list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime)
    ])
    def test_recall_by_during(self, startTime, endTime):
        '''recall页面时间段查询，查询时间默认为2022-01-01至今天'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s;"
        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 00:00:00', ENDtime,'ORDERED','PARTIALLY_SUBMITTED','SUBMITTED','PRINTED','PARTIALLY_PAID','INITIAL','SERVED','DELIVERED')
    ])
    def test_recall_by_status(self, startTime, endTime,*orderStatus):
        '''recall页面按状态查询，默认状态为未支付'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,
                                                                     orderStatus=orderStatus)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()

        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and status in %s;"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime, (0,1,2,3,4,5,7,101) ))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime, 'DINE_IN')
    ])
    def test_recall_by_ordertype(self, startTime, endTime, orderType):
        '''recall页面按订单类型查询，默认类型为Dine_in'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,
                                                                     orderType=orderType)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and order_type='DINE_IN';"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime, 'CASH')
    ])
    def test_recall_by_paymenttype(self, startTime, endTime, paymentType):
        '''recall页面按付款类型查询，默认类型为现金'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,
                                                                     paymentType=paymentType)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where id in (select order_id from payment_record where created_on between %s and %s and payment_type='CASH') or  id in (select parent_order_id from order_bill where id in (select order_id from payment_record where created_on between %s and %s and payment_type='CASH') and parent_order_id is not NULL);"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime,'2022-01-01 06:00:00',ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime)
    ])
    def test_recall_by_discount(self, startTime, endTime):
        '''recall页面按折扣加收查询，默认查询为折扣'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=True)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and discount_id is not NULL;"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)


    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime,-2)
    ])
    def test_recall_by_driver(self, startTime, endTime,driverId):
        '''recall页面按司机查询，默认查询为No Driver'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,driverId=driverId)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and driver_id is null;"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime, 1)
    ])
    def test_recall_by_serverId(self, startTime, endTime, serverId):
        '''recall页面按企台查询，默认查询为Leo'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         serverId=serverId)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and server_id=1;"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 06:00:00', ENDtime, 'POS')
    ])
    def test_recall_by_productLine(self, startTime, endTime, productLine):
        '''recall页面按产品线查询，默认查询为POS'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         productLine=productLine)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and product_line=1;"

        cur.execute(sql, ('2022-01-01 06:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)



    @parameterized.expand([
        param(STARTtime, ENDtime)
    ])
    def test_recall_modify_driver(self, startTime, endTime):
        '''recall页面盘点模式下修改司机，默认查询为Lisa'''
        #获取新单状态的第一个订单id，以备后面修改司机传参
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         orderStatus='ORDERED')
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])
        orderid=orderIdList[0]

        #修改司机接口的入参
        createdatatemp={"order": {"id": orderid,"userId": "1","driverId": "78","totalTips": "0"},
                                     "updateOrderDetails": "false"}
        recallmodifydriver = self.client.service.BatchSaveOrder(orderRequest=createdatatemp,
                                                             fetchOrder=False)
        Resonserecallmodifydriver= fastest_object_to_dict(recallmodifydriver)

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select driver_id from order_bill where id=%s;"

        cur.execute(sql, (orderid))
        data=cur.fetchone()
        #关闭游标
        cur.close()
        #关闭数据库
        conn.close()
        #比较数据库中的司机id和传参是否一致
        self.assertEqual(data[0],78)


    @parameterized.expand([
        param(STARTtime, ENDtime)
    ])
    def test_recall_pay_by_cash(self, startTime, endTime):
        '''recall页面盘点模式下现金结账，默认单条'''
        #获取新单状态的第一个订单id，以备后面修改司机传参
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         orderStatus='ORDERED')
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderid=orderlist[0]['id']
        price=orderlist[0]['totalPrice']

        #现金结账接口的入参
        createdatatemp={"paymentRecordRequest": {"paymentRecord": {"userId": "1","orderId": orderid,"type": "CASH","amount": price,"paidAmount": price}},
                        "userAuth": {"userId": "1","sessionKey": self.sessionKey}}
        paymentRecordRequestTemp = createdatatemp["paymentRecordRequest"]
        userAuthTemp=createdatatemp["userAuth"]
        recallpaybycash = self.client.service.BatchSavePaymentRecord(paymentRecordRequest=paymentRecordRequestTemp,
                                                             userAuth=userAuthTemp)
        Resonserecallpaybycash= fastest_object_to_dict(recallpaybycash)
        self.assertEqual(True, Resonserecallpaybycash['paymentRecordResponse'][0]['Result']['successful'])


        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select status from order_bill where id=%s;"

        cur.execute(sql, (orderid))
        data=cur.fetchone()
        #关闭游标
        cur.close()
        #关闭数据库
        conn.close()
        #比较数据库中的司机id和传参是否一致
        print(data[0])
        self.assertEqual(data[0],'100')

    # @parameterized.expand([
    #     param(STARTtime, ENDtime)
    # ])
    # def test_recall_pay_by_cash(self, startTime, endTime):
    #     '''recall页面盘点模式下现金结账，默认单条'''
    #     # 获取新单状态的第一个订单id，以备后面修改司机传参
    #     resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
    #                                                                  filterByCharge=False, filterByDiscount=False,
    #                                                                  orderStatus='ORDERED')
    #     ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
    #     # self.loggper.info(ParsedResponseRecallByCharge['orders'])
    #     orderlist = ParsedResonseRecallByCharge['orders']
    #     orderid = orderlist[0]['id']
    #     price = orderlist[0]['totalPrice']
    #     price_add = price + 2
    #     #         #现金结账接口的入参
    #     #         createdatatemp={"orderId":"1318","fetchPayments":"true",
    #     #                         "userAuth":{"userId":"1","sessionKey":"flqlvme29gidksk5km4ouqp7pn"}
    #     # }
    #     # paymentRecordRequestTemp = createdatatemp["paymentRecordRequest"]
    #     SavePaymentRecordtemp = {
    #         "paymentRecord": {"userId": "1", "orderId": orderid, "type": "CREDIT_CARD", "amount": price,
    #                           "paidAmount": price, "multiplePayments": "false", "cardType": "UNKNOWN"},
    #         "transactionDetail": {"actionType": "SALE_KEYED", "amount": price, "cardNumber": "4111111111111111",
    #                               "expirationDate": "923", "cardholder": "pp"},
    #         "userAuth": {"userId": "1", "sessionKey": self.sessionKey}
    #         }
    #     paymentRecordTemp = SavePaymentRecordtemp["paymentRecord"]
    #     transactionDetailtemp = SavePaymentRecordtemp["transactionDetail"]
    #     userAuthTemp = SavePaymentRecordtemp["userAuth"]
    #     recallpaybycash = self.client.service.SavePaymentRecord(printPaymentReceipt=True, merchantCopyOnly=False,
    #                                                             paymentRecord=paymentRecordTemp,
    #                                                             transactionDetail=transactionDetailtemp,
    #                                                             userAuth=userAuthTemp)
    #
    #     SavePaymentRecordjosn = fastest_object_to_dict(recallpaybycash)
    #     pay_id = SavePaymentRecordjosn["id"]
    #
    #     BatchSavePaymentRecordTemp = {
    #         "paymentRecordRequest": {"paymentRecord": {"userId": "1", "id": pay_id, "amount": price_add,
    #                                                    "paidAmount": price_add, "tipAmount": "2", "cashTipAmount": "0",
    #                                                    "tipAdded": "true"},
    #                                  "transactionDetail": {"actionType": "APPLY_TIP", "tipAmount": "2"}},
    #         "userAuth": {"userId": "1", "sessionKey": self.sessionKey}}
    #     paymentRecordRequestTemp = BatchSavePaymentRecordTemp["paymentRecordRequest"]
    #     userAuthTemp = BatchSavePaymentRecordTemp["userAuth"]
    #     BatchSavePaymentRecord = self.client.service.BatchSavePaymentRecord(
    #         paymentRecordRequest=paymentRecordRequestTemp,
    #         userAuth=userAuthTemp)
    #
    #     BatchSavePaymentRecordjson = fastest_object_to_dict(BatchSavePaymentRecord)
    #     pay_id1 = BatchSavePaymentRecordjson["paymentRecordResponse"][0]["id"]
    #     self.assertEqual(True, BatchSavePaymentRecordjson['paymentRecordResponse'][0]['Result']['successful'])
    #
    #     self.logger.info("enter recharge")
    #     conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
    #                            charset='utf8')
    #     # 获取游标
    #     cur = conn.cursor()
    #     sql = "select tip_amount from payment_record where id=%s;"
    #
    #     cur.execute(sql, (pay_id1))
    #     data = cur.fetchone()
    #     # 关闭游标
    #     cur.close()
    #     # 关闭数据库
    #     conn.close()
    #     # 比较数据库中的司机id和传参是否一致
    #     self.assertEqual(data[0], 2.0)

    @parameterized.expand([
        param(STARTtime, ENDtime)
    ])
    def test_recall_paytips_by_credit(self, startTime, endTime):
        '''recall页面信用卡添加小费'''
        #保存一个新单
        now = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                           "order": {"createTime": now, "callerId": "false", "type": "TOGO", "status": "ORDERED",
                                     "currentUserId": "1", "userId": "1", "taxExempt": "false", "numOfGuests": "1",
                                     "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.80", "roundingAmount": "0",
                                     "printTicketWhenVoid": "true", "orderTax": {"taxId": "1", "taxAmount": "0.8"},
                                     "discountID": "-1", "chargeID": "-1", "discount": "0", "charge": "0",
                                     "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                                               "orderItems": {"saleItemId": "924",
                                                                                              "seatId": "1",
                                                                                              "quantity": "1",
                                                                                              "originalSalePrice": "7.95",
                                                                                              "price": "7.95",
                                                                                              "status": "ORDERED",
                                                                                              "discount": "0",
                                                                                              "charge": "0"}}}}
        systemHandle = POSSystem()
        systemHandle.saveOrder(order_param)
        '''recall页面盘点模式下现金结账，默认单条'''
        # 获取新单状态的第一个订单id，以备后面修改司机传参
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         orderStatus='ORDERED')
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderid = orderlist[0]['id']
        price = orderlist[0]['totalPrice']
        price_add = price + 2
        #         #现金结账接口的入参
        #         createdatatemp={"orderId":"1318","fetchPayments":"true",
        #                         "userAuth":{"userId":"1","sessionKey":"flqlvme29gidksk5km4ouqp7pn"}
        # }
        # paymentRecordRequestTemp = createdatatemp["paymentRecordRequest"]
        SavePaymentRecordtemp = {
                "paymentRecord": {"userId": "1", "orderId": orderid, "type": "CREDIT_CARD", "amount": price,
                                  "paidAmount": price, "multiplePayments": "false", "cardType": "UNKNOWN"},
                "transactionDetail": {"actionType": "SALE_KEYED", "amount": price, "cardNumber": "4111111111111111",
                                      "expirationDate": "923", "cardholder": "pp"},
                "userAuth": {"userId": "1", "sessionKey": self.sessionKey}
                }
        paymentRecordTemp = SavePaymentRecordtemp["paymentRecord"]
        transactionDetailtemp = SavePaymentRecordtemp["transactionDetail"]
        userAuthTemp = SavePaymentRecordtemp["userAuth"]
        recallpaybycash = self.client.service.SavePaymentRecord(printPaymentReceipt=True, merchantCopyOnly=False,
                                                                    paymentRecord=paymentRecordTemp,
                                                                    transactionDetail=transactionDetailtemp,
                                                                    userAuth=userAuthTemp)

        SavePaymentRecordjosn = fastest_object_to_dict(recallpaybycash)
        pay_id = SavePaymentRecordjosn["id"]

        BatchSavePaymentRecordTemp = {
                "paymentRecordRequest": {"paymentRecord": {"userId": "1", "id": pay_id, "amount": price_add,
                                                           "paidAmount": price_add, "tipAmount": "2",
                                                           "cashTipAmount": "0",
                                                           "tipAdded": "true"},
                                         "transactionDetail": {"actionType": "APPLY_TIP", "tipAmount": "2"}},
                "userAuth": {"userId": "1", "sessionKey": self.sessionKey}}
        paymentRecordRequestTemp = BatchSavePaymentRecordTemp["paymentRecordRequest"]
        userAuthTemp = BatchSavePaymentRecordTemp["userAuth"]
        BatchSavePaymentRecord = self.client.service.BatchSavePaymentRecord(
                paymentRecordRequest=paymentRecordRequestTemp,
                userAuth=userAuthTemp)

        BatchSavePaymentRecordjson = fastest_object_to_dict(BatchSavePaymentRecord)
        pay_id1 = BatchSavePaymentRecordjson["paymentRecordResponse"][0]["id"]
        self.assertEqual(True, BatchSavePaymentRecordjson['paymentRecordResponse'][0]['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select tip_amount from payment_record where id=%s;"

        cur.execute(sql, (pay_id1))
        data = cur.fetchone()
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        # 比较数据库中的司机id和传参是否一致
        self.assertEqual(data[0], 2.0)