#coding=utf-8
import unittest
import pymysql
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client
import time

class LoyaltyCardTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    @parameterized.expand([
        param("799")
    ])
    def test_create_LoyaltyCard(self, loyaltyCardNo):
        """
        loyaltyCardNo: 会员卡ID
        创建礼品卡
        :return: None
        """
        self.logger.info("enter create")
        createuserApi = {"loyaltyCard": {"number": loyaltyCardNo, "expireTime": "2099-12-31",
                                         "enabled": "1", "customer": {"firstName": "chenchen"}},
                         "userAuth_loyalty": {"userId": "1", "sessionKey": self.sessionKey},}

        loyaltyCard_temp = createuserApi["loyaltyCard"]
        userAuth_loyalty_temp = createuserApi["userAuth_loyalty"]

        res_createLoyaltyCard = self.client.service.SaveLoyaltyCard(loyaltyCard=loyaltyCard_temp, userAuth=userAuth_loyalty_temp)
        ParsedResponseLoyaltyCard = fastest_object_to_dict(res_createLoyaltyCard)
        self.logger.info(ParsedResponseLoyaltyCard)
        self.assertEqual(True, ParsedResponseLoyaltyCard['successful'])

    def test_recharge_loyaltycard(self):
        # 查找会员卡
        # 连接数据库,查询要重置会员卡的id
        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from loyalty_card where number='23456';")
        # 礼品卡对应数据库字段 ： id, name, description, number, points, all_time_points, ytd_points, balance, active_time, expire_time,
        # issued_to, customer_id, enabled,created_on, last_updated, created_by, last_updated_by, version,
        # membership_level_id, discount_rate, cloud_membership_is,

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        for d in data:
          loyaltyCardId = d[0]
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"type": "LOYALTY_CARD", "totalPrice": 50, "userPassword": "11",
                       "orderItems":{"saleItemId": "791", "quantity": "1", "price": 50,
                                     "loyaltyCard":{"id": loyaltyCardId, "number": "23456"}}},}
        createuserApi = order_param
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        # 发送保存订单请求
        res = self.client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])
        targetId = ParsedResponse['Result']['id']
        # 锁定任务
        resAddLock = self.client.service.AddLock(type="ORDER", targetId=targetId, userAuth=userAuth_temp)
        ParsedResponseAddLock = fastest_object_to_dict(resAddLock)
        self.logger.info(ParsedResponseAddLock)
        self.assertEqual(True, ParsedResponseAddLock['Result']['successful'])

        # 现金支付
        respulCashDrawer = self.client.service.PullCashDrawer(userId=1, userAuth=userAuth_temp)
        ParsedResponsepulCashDrawer = fastest_object_to_dict(respulCashDrawer)
        self.logger.info(ParsedResponsepulCashDrawer)
        self.assertEqual(True, ParsedResponsepulCashDrawer['successful'])
        paymentRecord = {"userId":1, "orderId": targetId, "type": "CASH", "amount": 50,
                                          "paidAmount":50, "multiplePayments": "false"}

        resSavePaymentRecord = self.client.service.SavePaymentRecord(printPaymentReceipt=True, merchantCopyOnly= False,
                                                                     paymentRecord=paymentRecord,
                                                                     userAuth=userAuth_temp)

        ParsedResponseSavePaymentRecord = fastest_object_to_dict(resSavePaymentRecord)
        self.logger.info(ParsedResponseSavePaymentRecord)
        self.assertEqual(True, ParsedResponseSavePaymentRecord['Result']['successful'])
        # 解除锁定任务
        resRemoveLock = self.client.service.RemoveLock(type="ORDER", targetId=targetId, userAuth=userAuth_temp)
        ParsedResponseRemoveLock = fastest_object_to_dict(resRemoveLock)
        self.logger.info(ParsedResponseRemoveLock)
        self.assertEqual(True, ParsedResponseRemoveLock['Result']['successful'])

    def test_update_LoyaltyCard(self):
        # 更新名称为23456的会员卡的余额
        self.logger.info("enter update")
        createuserApi = {"userAuth_loyalty": {"userId": "1"},
                         "loyaltyCard":{"id": 3, "number": "23456", "balance": 300, "syncFromCloud": "false"}}
        userAuth_loyalty_temp = createuserApi["userAuth_loyalty"]
        loyalty_temp = createuserApi["loyaltyCard"]
        res = self.client.service.SaveLoyaltyCard(loyaltyCard=loyalty_temp, userAuth=userAuth_loyalty_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['successful'])

    def test_delete_LoyaltyCard(self):
        self.logger.info("enter delete")
        createuserApi = {"userAuth_loyalty": {"userId": "1"}, }
        userAuth_loyalty_temp = createuserApi["userAuth_loyalty"]

        # 连接数据库,查询到礼品卡在数据库存储的ID
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from loyalty_card where number='799';")
        # 会员卡对应数据库字段  id, name, description, number, points, all_time_points, ytd_points, balance, active_time, expire_time,
        # issued_to, customer_id, enabled,created_on, last_updated, created_by, last_updated_by, version,
        # membership_level_id, discount_rate, cloud_membership_is,

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        for d in data:
            loyaltyCardId = d[0]
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()

        # 删除礼品卡，ID属于必填字段
        res_deleteLoyaltyCard = self.client.service.DeleteLoyaltyCard(id=loyaltyCardId, cardNumber="799",
                                                                      userAuth=userAuth_loyalty_temp)
        ParsedResponseLoyaltyCard = fastest_object_to_dict(res_deleteLoyaltyCard)
        # self.logger.info(ParsedResponseGiftCard)
        self.assertEqual(True, ParsedResponseLoyaltyCard['successful'])
        time.sleep(10)

        # 连接数据库,查询到礼品卡在数据库存储的ID
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        cur.execute("select * from loyalty_card where number='799';")
        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        print(data)
        for d in data:
            # 获取礼品卡的是否Enable状态的标志位
            loyaltyCardFlag = d[12]

        self.assertEqual(0, loyaltyCardFlag)

        cur.execute("delete from loyalty_card where number='799';")
        conn.commit()
        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
