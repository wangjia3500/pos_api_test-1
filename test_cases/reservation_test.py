#coding=utf-8
import unittest
import pymysql
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client
import datetime

# 获取当前时间
now = datetime.datetime.now()
# 获取今天零点
fromDate = now - datetime.timedelta(hours=now.hour, minutes=now.minute, seconds=now.second,
                                    microseconds=now.microsecond)
# 获取明天的零点
toDate = now + datetime.timedelta(hours=23, minutes=59, seconds=59)

class ReservationTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    @parameterized.expand([
        param(fromDate, toDate)
    ])
    def test_findReservations_by_date(self, fromDate, toDate):
        # 新建预约，为后面查询接口做准备
        reservationTime = datetime.datetime.now() + datetime.timedelta(hours=1)
        createuserApi = {"reservation": {"dateTime": reservationTime, "firstName": "test-cc",
                                         "status": "RESERVED", "confirmation": "false"}}

        reservation_tmp = createuserApi["reservation"]
        resSaveReservation = self.client.service.SaveReservation(reservation=reservation_tmp)
        ParsedResponseSaveReservation = fastest_object_to_dict(resSaveReservation)
        self.logger.info(ParsedResponseSaveReservation)

        #查询预约
        resReservationByDate = self.client.service.FindReservations(fromDate=fromDate, toDate=toDate)
        ParsedResponseReservation = fastest_object_to_dict(resReservationByDate)
        # self.logger.info(ParsedResponseRecallByCharge['orders'])
        reservationlist = ParsedResponseReservation['reservations']
        reservationIdList = list()
        for i in range(len(reservationlist)):
            reservationIdList.append(reservationlist[i]['id'])
        reservationIdList.sort()
        self.logger.info(reservationIdList)
        self.assertEqual(True, ParsedResponseReservation['Result']['successful'])

        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取数据库中对应时间段的预约记录
        cur = conn.cursor()
        #筛选出指定时间段的预约
        sql = "select * from reservation where created_on between %s and %s"
        cur.execute(sql, (fromDate, toDate))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        reservationIdListInSql=list()
        # print(data)
        for i in range(len(data)):
            id = data[i][0]
            reservationIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(reservationIdList, reservationIdListInSql)

    reservationTime = datetime.datetime.now() + datetime.timedelta(hours=1)
    @parameterized.expand([
        param(reservationTime, "chenchen")
    ])
    def test_save_resrevation(self, reservationTime, reservationName):

        createuserApi = {"reservation": {"dateTime": reservationTime, "firstName": reservationName,
                                         "status": "RESERVED", "confirmation": "false"}}

        reservation_tmp = createuserApi["reservation"]
        resSaveReservation = self.client.service.SaveReservation(reservation=reservation_tmp)
        ParsedResponseSaveReservation = fastest_object_to_dict(resSaveReservation)
        self.logger.info(ParsedResponseSaveReservation)
        self.assertEqual(True, ParsedResponseSaveReservation['Result']['successful'])

    waitingTime = datetime.datetime.now() + datetime.timedelta(hours=1)
    @parameterized.expand([
        param(waitingTime, "chenchen")
    ])
    def test_save_waitingList(self, waitingTime, waitingName):

        createuserApi = {"waitingList": {"dateTime": waitingTime, "firstName": waitingName,
                                         "status": "WAITING", "confirmation": "false"},
                         "userAuth_WaitingList": {"userId": "1", "sessionKey": self.sessionKey},
                         }

        waiting_tmp = createuserApi["waitingList"]
        userAuth_waitingList_temp = createuserApi["userAuth_WaitingList"]
        resSaveWaiting = self.client.service.SaveWaitingList(waitingList=waiting_tmp,
                                                             userAuth=userAuth_waitingList_temp)
        ParsedResponseSaveWaiting = fastest_object_to_dict(resSaveWaiting)
        self.logger.info(ParsedResponseSaveWaiting)
        self.assertEqual(True, ParsedResponseSaveWaiting['Result']['successful'])

    @parameterized.expand([
        param(fromDate, toDate)
    ])
    def test_findWaitingList_by_date(self, fromDate, toDate):
        # 新建等位，为后面查询做准备
        waitingTime = datetime.datetime.now() + datetime.timedelta(hours=1)
        createuserApi = {"waitingList": {"dateTime": waitingTime, "firstName": "test-cc",
                                         "status": "WAITING", "confirmation": "false"},
                         "userAuth_WaitingList": {"userId": "1", "sessionKey": self.sessionKey},
                         }

        waiting_tmp = createuserApi["waitingList"]
        userAuth_waitingList_temp = createuserApi["userAuth_WaitingList"]
        resSaveWaiting = self.client.service.SaveWaitingList(waitingList=waiting_tmp,
                                                             userAuth=userAuth_waitingList_temp)
        ParsedResponseSaveWaiting = fastest_object_to_dict(resSaveWaiting)
        self.logger.info(ParsedResponseSaveWaiting)

        #查询等位
        resWaitingByDate = self.client.service.FindWaitingList(fromDate=fromDate, toDate=toDate)
        ParsedResponseWaiting = fastest_object_to_dict(resWaitingByDate)
        self.logger.info(ParsedResponseWaiting)
        waitinglist = ParsedResponseWaiting['waitingLists']
        waitingIdList = list()
        for i in range(len(waitinglist)):
            waitingIdList.append(waitinglist[i]['id'])
        waitingIdList.sort()
        self.logger.info(waitingIdList)
        self.assertEqual(True, ParsedResponseWaiting['Result']['successful'])

        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取数据库中对应时间段的等位记录
        # 筛选出指定时间段的等为
        cur = conn.cursor()
        sql = "select * from waiting_list where created_on between %s and %s"
        cur.execute(sql, (fromDate, toDate))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        waitingIdListInSql=list()
        # print(data)
        for i in range(len(data)):
            id = data[i][0]
            waitingIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(waitingIdList, waitingIdListInSql)
